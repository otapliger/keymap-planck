#include QMK_KEYBOARD_H
#include "muse.h"

#define _LAYER0 0
#define _LAYER1 1
#define _LAYER2 2
#define _LAYER3 3
#define _LAYER4 4
#define _LAYER5 5

/* ENTER when tapped / SHIFT when held */
#define KC_ENTER_SHIFT MT(MOD_RSFT, KC_ENT)

/* LT(layer, kc)
    Momentarily activates *layer* when held, and sends *kc* when tapped.
    Only supports layers 0-15. */

/* LM(layer, mod)
    Momentarily activates *layer*, but with modifier(s) *mod* active.
    Only supports layers 0-15 and the left modifiers: MOD_LCTL, MOD_LSFT, MOD_LALT, MOD_LGUI.
    These modifiers can be combined using bitwise OR, e.g. LM(_LAYER1, MOD_LCTL | MOD_LGUI). */

#define LAYER0 TO(_LAYER0)
#define LAYER1 MO(_LAYER1)
#define LAYER2 TO(_LAYER2)
#define LAYER3 TO(_LAYER3)
#define LAYER4 MO(_LAYER4)
#define LAYER5 MO(_LAYER5)

const uint16_t PROGMEM keymaps[][MATRIX_ROWS][MATRIX_COLS] = {

/* LAYER0
/-----------------------------------------------------------------------------------------------\
| Play  |   Q   |   W   |   E   |   R   |   T   |   Y   |   U   |   I   |   O   |   P   |  Bksp |
|-----------------------------------------------------------------------------------------------|
| Tab   |   A   |   S   |   D   |   F   |   G   |   H   |   J   |   K   |   L   |   ;   |   '   |
|-----------------------------------------------------------------------------------------------|
| Shift |   Z   |   X   |   C   |   V   |   B   |   N   |   M   |   ,   |   .   |  Up   | Enter |
|-----------------------------------------------------------------------------------------------|
| Ctrl  |  Alt  | Super |   -   |   L1  |     Space     |   =   |   /   | Left  | Down  | Right |
\-----------------------------------------------------------------------------------------------/ */
[_LAYER0] = LAYOUT_planck_mit(
    KC_MEDIA_PLAY_PAUSE, KC_Q, KC_W, KC_E, KC_R, KC_T, KC_Y, KC_U, KC_I, KC_O, KC_P, KC_BSPC,
    KC_TAB, KC_A, KC_S, KC_D, KC_F, KC_G, KC_H, KC_J, KC_K, KC_L, KC_SCLN, KC_QUOT,
    KC_LSFT, KC_Z, KC_X, KC_C, KC_V, KC_B, KC_N, KC_M, KC_COMM, KC_DOT, KC_UP, KC_ENTER_SHIFT,
    KC_LCTL, KC_LOPT, KC_LCMD, KC_MINS, LAYER1, KC_SPC, KC_EQL, KC_SLSH, KC_LEFT, KC_DOWN, KC_RGHT
),

/* LAYER1
/-----------------------------------------------------------------------------------------------\
|       |   1   |   2   |   3   |   4   |   5   |   6   |   7   |   8   |   9   |   0   |   Del |
|-----------------------------------------------------------------------------------------------|
|  `    |       |       |       |       |       |       |       |       |       |       |       |
|-----------------------------------------------------------------------------------------------|
| Shift |       |       |       |       |       |       |       |   [   |   ]   |  PgUp | Shift |
|-----------------------------------------------------------------------------------------------|
|       |       |  Esc  |       | TRNSF |     LAYER2    |       |   \   |  Home |  PgDn |   End |
\-----------------------------------------------------------------------------------------------/ */
[_LAYER1] = LAYOUT_planck_mit(
    KC_NO, KC_1, KC_2, KC_3, KC_4, KC_5, KC_6, KC_7, KC_8, KC_9, KC_0, KC_DEL,
    KC_GRV, KC_NO, KC_NO, KC_NO, KC_NO, KC_NO, KC_NO, KC_NO, KC_NO, KC_NO, KC_NO, KC_NO,
    KC_LSFT, KC_NO, KC_NO, KC_NO, KC_NO, KC_NO, KC_NO, KC_NO, KC_LBRC, KC_RBRC, KC_PGUP, KC_RSFT,
    KC_NO, KC_NO, KC_ESC, KC_NO, KC_TRNS, LAYER2, KC_NO, KC_BSLS, KC_HOME, KC_PGDN, KC_END
),

/* LAYER2
/-----------------------------------------------------------------------------------------------\
| L3    |  F1   |  F2   |  F3   |  F4   |       |       |       |       |       |       |  Exit |
|-----------------------------------------------------------------------------------------------|
|       |  F5   |  F6   |  F7   |  F8   |       |       |       |       |       |       |       |
|-----------------------------------------------------------------------------------------------|
|       |  F9   |  F10  |  F11  |  F12  |       |       |       |       |       |       |       |
|-----------------------------------------------------------------------------------------------|
| Ctrl  |  Alt  | Super |       |       |               |       |       | Super |  Alt  |  Ctrl |
\-----------------------------------------------------------------------------------------------/ */
[_LAYER2] = LAYOUT_planck_mit(
    LAYER3, KC_F1, KC_F2, KC_F3, KC_F4, KC_NO, KC_NO, KC_NO, KC_NO, KC_NO, KC_NO, LAYER0,
    KC_NO, KC_F5, KC_F6, KC_F7, KC_F8, KC_NO, KC_NO, KC_NO, KC_NO, KC_NO, KC_NO, KC_NO,
    KC_NO, KC_F9, KC_F10, KC_F11, KC_F12, KC_NO, KC_NO, KC_NO, KC_NO, KC_NO, KC_NO, KC_NO,
    KC_LCTL, KC_LOPT, KC_LCMD, KC_NO, KC_NO, KC_NO, KC_NO, KC_NO, KC_LCMD, KC_LOPT, KC_LCTL
),

/* LAYER3
/-----------------------------------------------------------------------------------------------\
| RGB   |       |       |       |       |       |       |       |       |       |       |  Exit |
|-----------------------------------------------------------------------------------------------|
|       |       |       |       |       |       |       |       |       |       |       |       |
|-----------------------------------------------------------------------------------------------|
| L4    |       |       |       |       |       |       |       |       |       |       | Reset |
|-----------------------------------------------------------------------------------------------|
| L5    |       |       |       |       |               |       |       |       |       |       |
\-----------------------------------------------------------------------------------------------/ */
[_LAYER3] = LAYOUT_planck_mit(
    RGB_TOG, KC_NO, KC_NO, KC_NO, KC_NO, KC_NO, KC_NO, KC_NO, KC_NO, KC_NO, KC_NO, LAYER0,
    KC_NO, KC_NO, KC_NO, KC_NO, KC_NO, KC_NO, KC_NO, KC_NO, KC_NO, KC_NO, KC_NO, KC_NO,
    LAYER4, KC_NO, KC_NO, KC_NO, KC_NO, KC_NO, KC_NO, KC_NO, KC_NO, KC_NO, KC_NO, QK_BOOTLOADER,
    LAYER5, KC_NO, KC_NO, KC_NO, KC_NO, KC_NO, KC_NO, KC_NO, KC_NO, KC_NO, KC_NO
),

/* LAYER4
/-----------------------------------------------------------------------------------------------\
|       |       |       |       |       |       |       |       |       |       |       |       |
|-----------------------------------------------------------------------------------------------|
|       |       |       |       |       |       |       |       |       |       |       |       |
|-----------------------------------------------------------------------------------------------|
| TRNSF |       |       |       |       |       |       |       |       |       |       |       |
|-----------------------------------------------------------------------------------------------|
|       |       |       |       |       |               |       |       |       |       |       |
\-----------------------------------------------------------------------------------------------/ */
[_LAYER4] = LAYOUT_planck_mit(
    KC_NO, KC_NO, KC_NO, KC_NO, KC_NO, KC_NO, KC_NO, KC_NO, KC_NO, KC_NO, KC_NO, KC_NO,
    KC_NO, KC_NO, KC_NO, KC_NO, KC_NO, KC_NO, KC_NO, KC_NO, KC_NO, KC_NO, KC_NO, KC_NO,
    KC_TRNS, KC_NO, KC_NO, KC_NO, KC_NO, KC_NO, KC_NO, KC_NO, KC_NO, KC_NO, KC_NO, KC_NO,
    KC_NO, KC_NO, KC_NO, KC_NO, KC_NO, KC_NO, KC_NO, KC_NO, KC_NO, KC_NO, KC_NO
),

/* LAYER5
/-----------------------------------------------------------------------------------------------\
|       |       |       |       |       |       |       |       |       |       |       |       |
|-----------------------------------------------------------------------------------------------|
|       |       |       |       |       |       |       |       |       |       |       |       |
|-----------------------------------------------------------------------------------------------|
|       |       |       |       |       |       |       |       |       |       |       |       |
|-----------------------------------------------------------------------------------------------|
| TRNSF |       |       |       |       |               |       |       |       |       |       |
\-----------------------------------------------------------------------------------------------/ */
[_LAYER5] = LAYOUT_planck_mit(
    KC_NO, KC_NO, KC_NO, KC_NO, KC_NO, KC_NO, KC_NO, KC_NO, KC_NO, KC_NO, KC_NO, KC_NO,
    KC_NO, KC_NO, KC_NO, KC_NO, KC_NO, KC_NO, KC_NO, KC_NO, KC_NO, KC_NO, KC_NO, KC_NO,
    KC_NO, KC_NO, KC_NO, KC_NO, KC_NO, KC_NO, KC_NO, KC_NO, KC_NO, KC_NO, KC_NO, KC_NO,
    KC_TRNS, KC_NO, KC_NO, KC_NO, KC_NO, KC_NO, KC_NO, KC_NO, KC_NO, KC_NO, KC_NO
)

};

layer_state_t layer_state_set_user(layer_state_t state) {
    switch (get_highest_layer(state)) {
    case _LAYER0:
        rgblight_setrgb(0x00, 0x00, 0x00);
        break;
    case _LAYER2:
        rgblight_setrgb(0x00, 0x00, 0xff);
        break;
    case _LAYER3:
        rgblight_setrgb(0xff, 0x00, 0x00);
        break;
    case _LAYER4:
        rgblight_setrgb(0xff, 0x00, 0x00);
        break;
    case _LAYER5:
        rgblight_setrgb(0xff, 0x00, 0x00);
        break;
    default:
        rgblight_setrgb(0x00, 0x00, 0x00);
        break;
    }
    return state;
}

#if defined(ENCODER_MAP_ENABLE)
const uint16_t PROGMEM encoder_map[][NUM_ENCODERS][NUM_DIRECTIONS] = {
    [_LAYER0] = { ENCODER_CCW_CW(KC_VOLD, KC_VOLU) },
    [_LAYER1] = { ENCODER_CCW_CW(KC_VOLD, KC_VOLU) },
    [_LAYER2] = { ENCODER_CCW_CW(KC_VOLD, KC_VOLU) },
    [_LAYER3] = { ENCODER_CCW_CW(RGB_VAD, RGB_VAI) },
    [_LAYER4] = { ENCODER_CCW_CW(RGB_HUD, RGB_HUI) },
    [_LAYER5] = { ENCODER_CCW_CW(RGB_SAD, RGB_SAI) },
};
#endif
